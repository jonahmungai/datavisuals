from jose import JWTError, jwt
from datetime import datetime, timedelta
import schemas,database,models
from fastapi import Depends, status, HTTPException
from fastapi.security import OAuth2PasswordBearer
from sqlalchemy.orm import Session
from config import Settings
from requests import Request

from typing import ClassVar
import hmac,base64,struct,hashlib,time


# install python_jose and not jose to avoid errors in jose
# this code is used to create a token which expires to ensure a user is not logged in forever






settings=Settings()

SECRET_KEY = settings.secret_key
ALGORITHM = settings.algorithm
ACCESS_TOKEN_EXPIRE_MINUTES = settings.access_token_expire_minutes 

TOTP_SECRET=settings.secret

oath2_scheme = OAuth2PasswordBearer(tokenUrl="login")




def create_access_token(data: dict):

    to_encode = data.copy()

    expire = datetime.utcnow() + timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    to_encode.update({"exp": expire})

    encoded_jwt = jwt.encode(to_encode, SECRET_KEY,algorithm= ALGORITHM)

    return encoded_jwt


def verify_access_token(token: str, credentials_exception):
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])

        print(payload)
        x=type(payload)
        print ( x)

        id: int = payload.get("user_id")

        print("id is",id)

        if id is None:
            print('no id')
            raise credentials_exception
           
        
        print ("tokendata",schemas.TokenData)
        token_data = schemas.TokenData(id=id)


    except JWTError:
        print ('jwt error')
        raise credentials_exception
        

    return token_data


def get_current_user(token: str = Depends(oath2_scheme),db: Session = Depends(database.get_db)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail=f"Invalid Login Details" + '' + "PLEASE LOGIN " ,
        headers={"WWW-Authenticate":"Bearer"}
    )
    #if credentials_exception:
      #  print(request.headers)

    token = verify_access_token(token, credentials_exception)

    user= db.query(models.User).filter(models.User.id == token.id).first()

    
    return user



# password change 

def get_hotp_token(secret,intervals_no):
    key=base64.b32decode(secret,True)

    #decoding our  key
    msg=struct.pack(">Q",intervals_no)

    #conversations btwn python vals and C structs  rep

    h=hmac.new(key,msg,hashlib.sha1).digest()
    O=O=h[19]& 15

    # generate hash using both hashing algo is HMAC

    h=(struct.unpack(">I",h[O:O+4])[0]& 0x7fffffff)% 1000000

    # unpacking

    return h

def get_totp_token(secret):
    #ensuring to give same otp for 30 secs
    x=str(get_hotp_token(secret,intervals_no=int(time.time())//30 ))
    # adding 0 in beginning till otp has 6n digits

    while len(x)!=6:
        x+='0'
        return x
    
    # base 664 encoded key



print (get_totp_token(TOTP_SECRET))