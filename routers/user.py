
import datetime
import email
from http import HTTPStatus
from multiprocessing import synchronize
from operator import le
from this import d
from urllib import request
from psycopg2 import IntegrityError
import pydantic
from fastapi import FastAPI,Depends, Response,status,HTTPException,APIRouter,Form,Request
from pydantic import BaseModel, EmailStr
import database,schemas,models,utils,main
from database import get_db,engine
from typing import Optional,List
from sqlalchemy.orm import session
from pydantic import BaseModel 
from pydantic.config import ConfigDict
import ouath2
from fastapi.templating import Jinja2Templates
from fastapi.responses import HTMLResponse

import random
import secrets
import string
from random import choice

from typing import Dict

from collections import defaultdict

router=APIRouter(tags=['Users'])
p={"x","val"}
#reset_codes:Dict[str,int]={}
codes=[{"name":"james@gmail.com" ,"rest_code":"12345"},{"name":"jimmy@gmail.com","rest_code":"17345"}]

templates=Jinja2Templates(directory="templates/")
@router.get("/Users/{id}")
def get_user(id:int,db:session=Depends(get_db)):  
    user= db.query(models.User).filter(models.User.id== id).first()
    
    
    if  not user:
        
      raise HTTPException(status_code=status.HTTP_404_NOT_FOUND ,detail=f"user with id:{id} was not found")
                                                                                                                                                                                                                                                                  
    return {"user_id":id,"email":user.email}

@router.get("/register",response_class=HTMLResponse)
def login(request:Request):
    context={'request':request}

    return templates.TemplateResponse("Registers.html", context)



@router.get("/Users/",response_model=List[schemas.UserOut])
def get_user(db:session=Depends(get_db)):  
    users= db.query(models.Users).all()
                                                                                                                                                                                                                                                     
    return users






@router.post("/Users",status_code=status.HTTP_201_CREATED,response_class=HTMLResponse)

async def create_User(request:Request,form_data=Depends(schemas.UserCreate.as_form_create_user),db:session=Depends(get_db)):
    # we create a new variable User because **dict method only takes one positional argument
    print(form_data)
    User:schemas.UserCreate=form_data
    #pasword_length :int=5
    #password_error=" password needs to be greater than 4"
    #if form_data.password<= pasword_length:
#
      #raise password_error
    hashed_password=utils.hash(form_data.password)
    form_data.password=hashed_password
  
    new_user=models.Users(**User.dict(exclude=['z','y']))#
    # .first()  must be used else db always returns tha user exists
    user_query= db.query(models.Users).filter(models.Users.email ==form_data.email).first()
    
    if user_query:
      error=f"user with email"+"" + ""+ form_data.email +"" +"already exists"

      raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail= error)

    db.add(new_user)
    db.commit()
    db.refresh(new_user)
    print(form_data)
    return templates.TemplateResponse("page.html", {"request":request})



class PasswordResetRequest(BaseModel):
   email:str


@router.patch("/password")
def get_code(request:Request,email:str,db:session=Depends(get_db)):
 


 if (email):
    user_check= db.query(models.User).filter(models.User.email ==email).first()

    if (user_check):
                 
                 reset_code=generate_reset_code()
                 print(reset_code)
                 #initialize an empty list that extracts all usernames present in codes  list 
                 x=[]
                 for code in codes:
                    v=code['name']
                    #print (v)

                    x.append(v)

                 print(x) 

                 z={"name":email,"rest_code":reset_code}
#{'name':'kagasi@gmail.com',"rest_code":"12346"}
                 #check if the new username is not  present in the x list which is a copy of original list. his is to avoid having two reset codes for single user
                 #if not present we add it to the original list 

                 if z["name"] not in x:
                     print(x)
                     codes.append(z)

                     print(codes)                

                  #if it is preent we only change the reset code 

                 if z["name"] in x:
                     
                     for code in codes:
                    #if email in code ['name']:
                        if code ["name"]==email:
                            print("user in")

                            print("users details " ,code)
                            code["rest_code"]=reset_code

                            print("users  new details " ,code)
                            break
                     
                  

                 #for code in codes:
                #  print(code["name"])
                  #   x=[]
                    # x.append(code['name'])


                 
                # new_code={"name":email,"rest_code":reset_code}

                 #codes.append(new_code)


                 
                  #reset_codes[email]=r
                               

                 #print(codes)

              
                 context={ "request":request,"reset_code":reset_code}

                 

                 return templates.TemplateResponse("passwordReset.html", context)

  
            

    else:
       

       
       return "email incorrect or user does not exist"
 

    
 else :
    raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail= "NO USERNAME")
 

class PasswordUpdate(BaseModel):
   email:str
   reset_code:int
   password:str




@router.patch("/password/reset")
def change_password(user_details:schemas.PasswordUpdate,db:session=Depends(get_db)):
    user_confirm= db.query(models.User).filter(models.User.email ==user_details.email)

    if (user_confirm):



      #return "user password successfully changed"
       
                   for code in codes:
                    #if email in code ['name']:
                        if code ["name"]==user_details.email:
                            print("user in")                          
                            dict_reset_code=code["rest_code"]
                            print(dict_reset_code)
                            print(user_details.reset_code)

                          # check if the user entered reset code is equal to reset code stored and sent to user

                            if dict_reset_code==user_details.reset_code:
                              print(user_details.dict())
                              hashed_password=utils.hash(user_details.password)
                              user_details.password=hashed_password
                              user_confirm.update(user_details.dict(exclude={'reset_code'},exclude_unset=True),synchronize_session=False)
                              db.commit()
                                
                              #print("users  new details " ,code)
                              return "user password successfully changed"
                            

                            else :
                                if dict_reset_code !=user_details.reset_code:
                                
                                 print('chek the reset code and try again')

                                 break
                    
                      #if email getting checked for is not equal break

                        else:
                            
                            if code ["name"]!=user_details.email:
                            
                              print("username in email not equal to stored username")
                            
                              continue                           
           
    else:     
         raise HTTPException(status_code=401 ,detail="Invalid")
       
           
       
      # {
     
          
      
          

          
       
    #}
       

   

   
  
   
   
   

   


    #return Response( 
  #except:

    
    #"new_user id":new_user.id}