
from typing_extensions import Self
from urllib import response
from fastapi import Depends, Response,status,HTTPException,APIRouter,Form,UploadFile,File,Request,Response
from pydantic import EmailStr
import database,models,schemas,utils,ouath2
from sqlalchemy.orm import session
from fastapi.security import OAuth2PasswordRequestForm
from fastapi.templating import Jinja2Templates

from fastapi.responses import HTMLResponse
import uvicorn
# this code is used to log in a user
router=APIRouter(tags=['Authentication'])
templates=Jinja2Templates(directory="templates/")




@router.get("/loginpage",response_class=HTMLResponse)
def login(request:Request):
    context={'request':request}
    return templates.TemplateResponse("log.html", context)
 
@router.post("/login",response_class=HTMLResponse)
def login(request:Request, response:Response, username: str = Form(...), password: str = Form(...), db:session=Depends(database.get_db)):
    
    user = db.query(models.Users).filter(models.Users.email == username).first()

    print(" Entered username:",username,  "Entered Password:",password)
     
    if not user:
         
          user = db.query(models.Users).filter(models.Users.username == username).first()



          if not user:
               raise HTTPException(status_code=status.HTTP_403_FORBIDDEN,detail=f"invalid login credentials."+" user was not found" "PLEASE  LOGIN AGAIN")
     
     
    if not utils.verify(password,user.password):

         raise HTTPException(status_code=status.HTTP_403_FORBIDDEN,detail="invalid login credentials." + "" +"PLEASE  LOGIN AGAIN" )
# create a token
    access_token=ouath2.create_access_token(data={"user_id":user.id})
  #  userrname:str=None
    
    print(access_token)
   

    
    response=templates.TemplateResponse("page.html",{'request':request,"token":access_token,"token_type":"bearer","userrname":"kamau"})
    
   # response.set_cookie(key="Authorization",value=f"Bearer{access_token}",httponly=True)

    #print(request.headers)


    
    

    return response

    
    
#return{"access_token
    