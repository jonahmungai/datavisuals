from fastapi import FastAPI,Depends, Response,status,HTTPException,APIRouter,Form,Request
from pydantic import BaseModel, EmailStr
import database,schemas,models,utils,main
from database import get_db,engine
from typing import Optional,List
from sqlalchemy.orm import session
from pydantic import BaseModel 
from pydantic.config import ConfigDict
import ouath2
from fastapi.templating import Jinja2Templates
from fastapi.responses import HTMLResponse




router=APIRouter(tags=['Products'])


templates=Jinja2Templates(directory="templates/")

@router.get("/products",response_class=HTMLResponse)

def  broad_prods(request: Request):

    #return "enter broad category"
    return templates.TemplateResponse("prod_category.html",context= {"request": request})#



