from http import HTTPStatus
from multiprocessing import context, synchronize
from fastapi import FastAPI,Depends, Response,status,HTTPException

from pydantic import BaseModel
import database,schemas,models,utils,ouath2
from database import get_db,engine
from typing import Optional,List
from sqlalchemy.orm import session
from fastapi.middleware.cors import CORSMiddleware
from fastapi.templating import Jinja2Templates


#from server import sockets,client




# import drive  hytmx   key in redis ,postgres  

from routers import user,auth,shop_products   #,chats chat
#from mpesa import pesa
#models.base
from fastapi import Depends, Response,status,HTTPException,APIRouter
import database,models,schemas,utils,ouath2
from sqlalchemy.orm import session
from fastapi.templating import Jinja2Templates
from fastapi import Request
import uvicorn
from fastapi.responses import HTMLResponse



models.Base.metadata.create_all(bind=database.engine)



app = FastAPI()
#gunicorn main:app --workers 2 --worker-class uvicorn.workers.UvicornWorker --bind 0.0.0.0:80

app.add_middleware(
   CORSMiddleware,
    allow_origins=['*'],
    allow_methods=['*'],
    allow_headers=['*']

)
router=APIRouter()

#app.include_router(pesa.router)
#app.include_router(post.router)
app.include_router(user.router)
app.include_router(auth.router)

app.include_router(shop_products.router)






templates=Jinja2Templates(directory="templates/")


@app.get("/login",response_class=HTMLResponse)
def login(request: Request):
    return templates.TemplateResponse("log.html",context= {"request": request})

    
@app.get("/",response_class=HTMLResponse)
def login(request: Request):
    return templates.TemplateResponse("index.html",context= {"request": request})



if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8000)

