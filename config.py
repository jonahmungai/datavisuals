
from pydantic_settings import BaseSettings
from pydantic import AnyUrl

class Settings(BaseSettings):

    database_name:str
    database_port:str
    algorithm:str
    access_token_expire_minutes:int
    database_username:str
    database_password:str
    secret_key:str
    database_hostname:str

    # secret for totp
    secret:str

    USE_NGROK:bool
    BASE_URL:AnyUrl


    class Config:
              env_file=".env"

settings=Settings()

            


              






