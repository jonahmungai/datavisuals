
from time import timezone
from sqlalchemy.orm import relationship
import typing
from typing_extensions import Dict

from sqlalchemy import  Column, ForeignKey,Integer,String, DOUBLE_PRECISION, Boolean, true,TIMESTAMP,DATE,Date,DECIMAL
from database import Base
from sqlalchemy.sql.expression import Null
from typing import Optional
from sqlalchemy import DOUBLE
import sqlalchemy as sa
from sqlalchemy.orm import Mapped,column_keyed_dict
from sqlalchemy.orm import mapped_column
# defining aschema with pydantic to ensure client only sends data we 

 
    #published=Column(Boolean,default=True)
 

class Users(Base):
    
    __tablename__ ="users"
    id=Column(Integer ,primary_key=True,nullable=False)
    email=Column(String,nullable=False,unique=True)
    first_name=Column(String,nullable=False)
    gender=Column(String,nullable=False)
    last_name=Column(String,nullable=False)
    date_of_birth=Column(sa.DATE,nullable=False)
    username=Column(String,nullable=False,unique=True) #unique
    password=Column(String,nullable=False)
    created_at=Column(sa.TIMESTAMP(timezone=True),server_default=sa.func.now(),  nullable=False)
    is_active=Column(Boolean,default=False)
    last_login=Column(sa.TIMESTAMP(timezone=True),server_default=sa.func.now(),nullable=False)
   # children= relationship()

  #contact:str

    #published=Column(Boolean,default=True)
    #created_at=Column(TIMESTAMP(timezone=True),server_default=("now()"),nullable=False)

class Location(Base):
    
    __tablename__ ="location"
    id=Column(Integer ,primary_key=True,nullable=False)
    county=Column(String,nullable=False)
    county_code=Column(Integer ,nullable=False)
    constituency=Column(String,nullable=False)
    ward=Column(String,nullable=False,unique=True)
    




class UserAddress(Base):
    __tablename__ = "useraddress"
    id=Column(Integer ,primary_key=True,nullable=False)
    user_id = Column(Integer,ForeignKey("users.id", ondelete="CASCADE",onupdate="CASCADE"),nullable=False)
    location_id= Column(Integer,ForeignKey("location.id", ondelete="CASCADE"),nullable=False)
    address =Column(String,nullable=False)
    contact_phone =Column(String,nullable=False)




#class UserRole(Base):
  #  __tablename__ = "account_user_role"
   # user_id = Column()
   # role_id = Column()

#class Role(Model):
 #   __tablename__ = "account_role"
 #   name = Column(db.String(80), unique=True)
  #  permissions = Column(db.Integer(), default=Permission.LOGIN)

class ProductCategory(Base):
    __tablename__ = "productcategory"

    id=Column(Integer,primary_key=True,nullable=False)
    #id pkey

    broad_item_cattegory = Column(String,unique=True,nullable=False)#kitchen , electronics ,clothes
   

class ProductDetails(Base):
    __tablename__ = "productdetails"
    id=Column(Integer,primary_key=True,nullable=False)

    #id pkey
    specific_item_cattegory_name= Column(String,nullable=False)# tv radio trousers ,
    product_category = Column(Integer,ForeignKey("productcategory.id", ondelete="CASCADE",onupdate="CASCADE"),nullable=False)
    #total_items_quantity=models.IntegerField()# 5tv  3 10 cables
    
    #getting category of product via relationship  one to many
    #product_details:Mapped["Product"]= relationship("Product",back_populates="productdetails")


class Product(Base):
    __tablename__ = "product"
    id=Column(Integer,primary_key=True,nullable=False)
    product_name = Column(String(255), nullable=False)
    #on_sale = Column(Boolean(), default=True)
    rating = Column(DECIMAL(8, 2), default=1.0)
    sold_count = Column(Integer, default=0)
    review_count = Column(Integer, default=0)
    product_price = Column(DECIMAL(10, 2))
   # category_id = Column(Integer)
    is_available = Column(Boolean, default=False)
    product_total_quantity = Column(Integer,nullable=False)
    description = Column(String,nullable=False)

    product_size = Column(String,nullable=True,default="n/a")
    product_colour= Column(String,default="n/a")
    brand = Column(String,nullable=True,default="n/a")
#get produc details every time you reurn product
   # productdetails:Mapped["ProductDetails"]= relationship("ProductDetails",back_populates="product_details")
# one to many relationship  product and product details

    product_details=Column(Integer ,ForeignKey("productdetails.id", ondelete="CASCADE",onupdate="CASCADE"),nullable=False)
    
   # images:Mapped[Dict] = relationship("c",back_populates="House")
    images:Mapped["Images"]= relationship("Images", back_populates="product")


  
class Images(Base):
    __tablename__ = "product_image"
    id=Column(Integer,primary_key=True,nullable=False)
    image = Column(String(255))
    product:Mapped["Product"] =relationship("Product", back_populates="images")
      #shipping_address = Column(db.String(255))
    product_id = Column(Integer,ForeignKey("product.id", ondelete="CASCADE",onupdate="CASCADE"),nullable=False)




class Order(Base):
    __tablename__ = "order"
    id=Column(Integer,primary_key=True,nullable=False)
   # token = Column(db.String(100), unique=True)
    #shipping_address = Column(db.String(255))
    user_id = Column(Integer,ForeignKey("users.id", ondelete="CASCADE",onupdate="CASCADE"),nullable=False)
    discount_amount = Column(DECIMAL(10, 2), default=0)
    discount_name = Column(String(100))
    #voucher_id = Column(Integer())
    shipping_price_net = Column(DECIMAL(10, 2))
    status = Column(String,default="not yet completed")
    #shipping_method_name = Column(db.String(100))
    total_net = Column(DECIMAL(10, 2))
    #shipping_method_id = Column(db.Integer())
    ship_status = Column(String,default="awaiting order completion")



class StockPurchases(Base):
    __tablename__ = "stockpurchases"
    id=Column(Integer,primary_key=True,nullable=False)
# this class extends products class
    #id pkey
    # tv radio trousers ,
   # 5tv  3 10 cables  
    product=Column(Integer,ForeignKey("product.id", ondelete="CASCADE",onupdate="CASCADE"),nullable=False)
    #product_name
    purchase_price= Column(DOUBLE_PRECISION,default=0.00)
    product_name = Column(String,default="please enter product name")
    products_id =Column(Integer ,default=2)# 3editable false
    date_purchased=Column(sa.TIMESTAMP(timezone=True),server_default=sa.func.now(),nullable=False)
    total_purchased_quantity=Column(Integer)
    
    #purchased_by = models.ForeignKey(CustomUser,on_delete=models.CASCADE,default=1)#

    #def save(self, *args, **kwargs):

        #self.update_stock_purchases()
        
     
      #  self.product_name = self.P.product_name # getting name of product from product class
       # print("name",self.product.product_name)
       # print("id",self.product.id)
       # self.products_id = self.product.id

       # self.product.total_product_quantity += self.total_purchased_quantity
      #  print("new product quantity after purchase",Product.total_product_quantity)
        
        

       #refernce  https://docs.djangoproject.com/en/4.2/ref/models/querysets/   check update
        #updating new Products.total_product_quantity after purchase
        #Product.prooducts.filter(id=self.products_id).update(total_product_quantity=self.product.total_product_quantity)
        

       # super().save(*args, **kwargs)#saving stocksales data

        

class StockSales(Base):
    __tablename__ = "stocksales"
    id=Column(Integer,primary_key=True,nullable=False)
# this class extends products class
    #id pkey
    # tv radio trousers ,
   # 5tv  3 10 cables  
    product=Column(Integer,ForeignKey("product.id", ondelete="CASCADE",onupdate="CASCADE"),nullable=False)
    #product_name
    sales_price= Column(DOUBLE_PRECISION,default=0.00)
    product_name = Column(String,default="please enter product name")
    products_id =Column(Integer ,default=2) 
    sales_time=Column(sa.TIMESTAMP(timezone=True),server_default=sa.func.now(),nullable=False)
    total_sales_quantity=Column(Integer,nullable=False)   