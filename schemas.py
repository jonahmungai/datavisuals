
from fastapi import Form
from typing import ClassVar
from typing import Any,Dict
from datetime import date, datetime

from pydantic import BaseModel,EmailStr, conint
import pydantic
from sqlalchemy import Boolean,Numeric,DOUBLE_PRECISION,Float,DATE,Date
from typing import Optional
from sqlalchemy import DOUBLE
from datetime import datetime
import sqlalchemy as sa




class PostBase(BaseModel):
    
    title:str
    content:str
    #published:bool= True
    

class PostCreate(PostBase):

    pass
class UserOut (BaseModel):

    email:EmailStr
    id:int
    created_at:datetime

    class config:
        from_attributes=True
        

class Post(PostBase):
    id:int
    owner_id:int
    #owner:UserOut
    content: str
    created_at:datetime
    
    class Config:
        from_attributes=True



class PostOut(BaseModel):
    Post:Post
    
    class Config:
        from_attributes=True
        
class UserCreate(BaseModel):
     #  for pydantic 2.3   . pydantic>networks.py function validate email  change line email = parts.normalized to parts.emailcause normalized is not defined
    email:EmailStr
    password:str
   # id:Optional[int]
    #contact:str
    first_name:str
    last_name:str
    date_of_birth:date
    username:str
    gender:str

    z:datetime=datetime.utcnow().date()

    y:str=str(z)

    print(y)
    print('type', type(y))
    


    @classmethod
    def as_form_create_user(cls,email:EmailStr=Form(...),password:str=Form(...),first_name:str=Form(...),
                            last_name:str=Form(...),date_of_birth:str=Form(default=y),username:str=Form(...),gender:str=Form(...)):

      return cls(date_of_birth=datetime.strptime(date_of_birth,'%Y-%m-%d'),email=email,password=password,first_name=first_name,last_name=last_name ,username=username,gender=gender)


    class Config:
        arbitrary_types_allowed=True
        from_attributes=True 

        
class LoginFormData(BaseModel):
    email:EmailStr
    password:str
    #id:Optional[int]
    

    @classmethod
    def as_form(cls,email:EmailStr=Form(...),password:str=Form(...)):

      return cls(email=email,password=password)
    
   
     
class UserLogin(BaseModel):
    email:EmailStr
    password:str
        

class Token(BaseModel):
    access_token:str
    token_type:str


class TokenData(BaseModel):
    id:Optional[int]  # original id:Optional[str]  
    
class Vote(BaseModel):
    post_id:int
    dir:conint(le=1)


class Location(BaseModel):

    county:str
    county_code:int
    constituency:str
    ward:str

    # this enables conversion of list to dict . that avoids not a valid dict error
    # its important when you want to return a list of all items 
    
    class Config:
        from_attributes=True



    


class House(BaseModel):

    house_name:str
    #house_image:str
    house_size:str
    house_price:float
    vacant:int
    village:str
    house_description:Optional[str]
    vacant_avail_date:Optional[date]

   
    class Config:
        from_attributes=True

# extends the house model to include a field ward required to identify location
class HouseCreate(House):

    ward:str

    """
    z holds current date . y stores the convert of current date to string because vacant avail date field expects a string
    #  if user enters a date in expected string format it is converted to date object and added to database 
    # else if no date is entered y is taken as default date to avoid error of vacant_avail field expecting a string convertible to date.
    however it is not added to db it is excluded in **dict() together with z 

    """
    z:datetime=datetime.utcnow().date()

    y:str=str(z)


    @classmethod
    def as_form(cls,house_name:str=Form(...),  #,house_image:str=Form(...)
        house_size:str=Form(...),house_price:float =Form(...),
        vacant:int=Form(...),village:str =Form(...),ward:str=Form(...),house_description:str=Form(default=None),vacant_avail_date=Form(default=y)):#,vacant_avail_date:datetime=Form(...)
      
        #avail_date=datetime.strptime(vacant_avail_date,'%d-%m-%y')

      return cls(house_name=house_name  ,ward=ward,   #,house_image=house_image,
        house_size=house_size.strip(),house_price=house_price,
        vacant=vacant,village=village,house_description=house_description,  vacant_avail_date=datetime.strptime(vacant_avail_date,'%Y-%m-%d').date())#house_description,vacant_avail_date=vacant_avail_date.date()


class Images(BaseModel):

    parent_id:int
   # house_size:str

    class Config:
        from_attributes=True

    @classmethod
    def as_form(cls,parent_id:int=Form(...) ):#,image_name:str=Form(...)  image_name=image_name,
        
        return cls(parent_id=parent_id)





class PasswordUpdate(UserCreate):
    email:EmailStr
    password:str
    reset_code:str


class HouseAmenities(BaseModel):

    house_amenity_id:Optional[int]
    amenity_1:str
    amenity_2:Optional[str]
    amenity_3:Optional[str]
#amenity_4:Optional[str]
  #  amenity_5:Optional[str]
 #   amenity_6:Optional[str]
 #   amenity_7:Optional[str]
   #3 amenity_8:Optional[str]
   # amenity_9:Optional[str]
   # amenity_10:Optional[str]


    class Config:
        from_attributes=True


   
    


    @classmethod                         
    def as_form(cls,house_amenity_id:int=Form(...),amenity_1=Form(...),amenity_2:Optional[str]=Form(default=None) ,
        


        amenity_3:Optional[str]=Form(default=None),amenity_4:Optional[str]=Form(default=None),   amenity_5:Optional[str]=Form( default=None),amenity_6:Optional[str]=Form(default=None)
        ,amenity_7:Optional[str]=Form(default=None),amenity_8:Optional[str]=Form(default=None),amenity_9:Optional[str]=Form(default=None),amenity_10:Optional[str]=Form( default= None)):#,image_name:str=Form(...)  image_name=image_name,
        
        return cls(house_amenity_id=house_amenity_id,amenity_1=amenity_1,amenity_2=amenity_2,
                   amenity_3=amenity_3,amenity_4=amenity_4,amenity_5=amenity_5,
                   amenity_6=amenity_6,amenity_7=amenity_7,amenity_8=amenity_8,amenity_9=amenity_9,amenity_10=amenity_10)



#


class HouseImages(BaseModel):

   # parent_id=Column(Integer ,ForeignKey("rentals.house_id", ondelete="CASCADE"),nullable=False)
    image_1:Optional[str]
    image_3:Optional[str]
    image_4:Optional[str]
    image_5:Optional[str]
    
    image_6:Optional[str]
    image_7:Optional[str]
    image_8:Optional[str]
    image_9:Optional[str]
    image_10:Optional[str]


    class Config:
        from_attributes=True



class HouseOut(House):

    amenities:HouseAmenities
   # print(amenities)


    class config:
         from_attributes=True








        
class ProductCategoryCreate(BaseModel):
     #  for pydantic 2.3   . pydantic>networks.py function validate email  change line email = parts.normalized to parts.emailcause normalized is not defined  
    broad_item_cattegory:str#kitchen , electronics ,clothes
   
    


    @classmethod
    def as_form_create_product_category(cls, broad_item_cattegory:str=Form(...)):
      return cls(broad_item_cattegory=broad_item_cattegory)


    class Config:
        arbitrary_types_allowed=True
        from_attributes=True 





       
class ProductDetailsCreate(BaseModel):
     #  for pydantic 2.3   . pydantic>networks.py function validate email  change line email = parts.normalized to parts.emailcause normalized is not defined  
    specific_item_cattegory_name:str#kitchen , electronics ,clothes
   
    


    @classmethod
    def as_form_create_product_details(cls, specific_item_cattegory_name:str=Form(...)):
      return cls(specific_item_cattegory_name=specific_item_cattegory_name)


    class Config:
        arbitrary_types_allowed=True
        from_attributes=True 

